inc = (payload = 1) => ({ type: 'INC', payload });
dec = (payload = 1) => ({ type: 'DEC', payload });
addTodo = (payload) => ({ type: 'ADD_TODO', payload });
removeTodo = (payload) => ({ type: 'REMOVE_TODO', payload });

initialState = {
    counter: 0,
    todos: [],
    placki: 123
};

[inc(), inc(2), addTodo('placki'), inc(), dec(2)].reduce(reducer, initialState)

function reducer(state, action) {
    switch (action.type) {
        case 'INC': return { ...state, counter: state.counter += action.payload }
        case 'DEC': return { ...state, counter: state.counter -= action.payload }
        case 'ADD_TODO': return { ...state, todos: [...state.todos, { name: action.payload }] }
        case 'REMOVE_TODO': return { ...state, todos: state.todos.filter(t => t.name !== action.payload) }
        default: return state;
    }
}
// {counter: 2, todos: Array(1), placki: 123}
state = initialState


state = reducer(state, addTodo('awesome'))
state
// {counter: 1, todos: Array(1), placki: 123}counter: 1placki: 123todos: [{…}]__proto__: Object

state = reducer(state, addTodo('awesome'))
state
// {counter: 1, todos: Array(2), placki: 123}

state = reducer(state, inc(23))
state
// {counter: 24, todos: Array(2), placki: 123}

// https://github.com/paularmstrong/normalizr



inc = (payload = 1) => ({ type: 'INC', payload });
dec = (payload = 1) => ({ type: 'DEC', payload });
addTodo = (payload) => ({ type: 'ADD_TODO', payload });
removeTodo = (payload) => ({ type: 'REMOVE_TODO', payload });

initialState = {
    counter: 0,
    todos: [],
    placki: 123
};

[inc(), inc(2), addTodo('placki'), inc(), dec(2)].reduce(reducer, initialState)

function counterReducer(state = 0, action){
    switch (action.type) {
        case 'INC': return  state + action.payload
        case 'DEC': return  state - action.payload  
		default: return state;
    }
}

function todosReducer(state = [], action){
    switch (action.type) {
        case 'ADD_TODO': return   [...state, { name: action.payload }] 
        case 'REMOVE_TODO': return state.filter(t => t.name !== action.payload)
		default: return state;
    }
}

function reducer(state, action) {
	return {
        ...state,
		counter: counterReducer(state.counter, action),
		todos: todosReducer(state.todos, action),
    }    
}
// {counter: 2, todos: Array(1), placki: 123}
state = initialState


state = reducer(state, addTodo('awesome'))
state