import { search } from "../services";
import React from "react";
import { Album } from "../common/models/Album";


type C = {
    query: string;
    results: Album[],
    onSearch(query: string): void
}

export const searchContext = React.createContext<C>({
    query: '',
    results: [],
    onSearch() { throw 'No <searchContext.Provider> found' }
})

export class MusicProvider extends React.Component {

    state = {
        query: '', results: [
            {
                id: '123',
                name: 'Placki', images: [
                    {
                        url: 'http://placekitten.com/300/300', width: 300, height: 300
                    }
                ]
            },
            {
                id: '234',
                name: 'Placki 234', images: [
                    {
                        url: 'http://placekitten.com/300/300', width: 300, height: 300
                    }
                ]
            },
            {
                id: '345',
                name: 'Placki 345', images: [
                    {
                        url: 'http://placekitten.com/300/300', width: 300, height: 300
                    }
                ]
            },
        ]
    }

    search = (query: string) => {
        search.searchAlbums(query).then(albums => {
            this.setState({ results: albums })
        })
    }

    render() {
        return <searchContext.Provider value={{
            onSearch: this.search,
            ...this.state
        }}>
            {this.props.children}
        </searchContext.Provider>
    }
}

export const withMusicContext = (Component: React.ComponentType<{ onSearch(query: string): void }>) => () =>
    <searchContext.Consumer>
        {(props) => <Component onSearch={props.onSearch}></Component>}
    </searchContext.Consumer>;