import { createStore, Reducer, combineReducers } from 'redux'
import { playlistsReducer } from './reducers/playlists';


export const rootReducer = combineReducers({
    playlists: playlistsReducer,
})

export const store = createStore(rootReducer)