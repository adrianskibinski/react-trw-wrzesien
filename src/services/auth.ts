export class Auth {


    constructor(
        private auth_url: string,
        private client_id: string,
        private redirect_uri: string,
        private response_type = 'token'
    ) {
        const jsonToken = sessionStorage.getItem('token')
        if (jsonToken) {
            this.token = JSON.parse(jsonToken)
        } else {
            const match = document.location.hash.match(/access_token=([^&]*)/)
            this.token = match && match[1]
            if (this.token) {
                sessionStorage.setItem('token', JSON.stringify(this.token))
                document.location.hash = ''
            }

        }
    }

    authorize() {
        sessionStorage.removeItem('token')
        const url = `${this.auth_url}?`
            + `client_id=${this.client_id}&`
            + `redirect_uri=${this.redirect_uri}&`
            + `response_type=${this.response_type}`;
        document.location.href = (url)
    }

    token: string | null = null

    getToken() {
        if (!this.token) {
            this.authorize()
        }
        return this.token
    }
}
