import { auth } from "../services";
import { AlbumsResponse } from "../common/models/Album";

export class MusicSearch {

    searchAlbums(query = 'batman') {
        /* axios */
        return fetch('https://api.spotify.com/v1/search?type=album&q=' + query, {
            headers: {
                Authorization: `Bearer ${auth.getToken()}`
            },
        })
            .then(r => r.status < 400 ? r : Promise.reject<Response>((r.status)))
            .then(v => v.json())
            .then((t: AlbumsResponse) => t.albums.items,
                (err) => {
                    auth.token = ''
                    auth.authorize()
                    return []
                    // return Promise.reject<Response>(new Error('placki'))
                })
    }
}