
export interface Entity {
    id: number | string
    name: string
}

export interface Playlist extends Entity {
    favorite: boolean
    /**
     * Hex color value
     */
    color: string
    // tracks: Array<Track>
    tracks?: Track[]
}

export interface Track extends Entity { }




