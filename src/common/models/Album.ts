export interface Entity {
    id: string;
    name: string;

}
export interface Album extends Entity {
    images: Image[];
}

export interface Artist extends Entity { }

export interface Image {
    url: string;
    height: number;
    width: number;
}

export interface PageObject<T> {
    items: T[];
    limit: number;
    // next?: any;
    // offset: number;
    // previous?: any;
    // total: number;
}

export type AlbumsResponse = {
    albums: PageObject<Album>
}

export type ArtistsResponse = {
    artists: PageObject<Artist>
}