import React from "react";
import { NavLink } from "react-router-dom";

type P = {
    title: string | React.ComponentType
    //React.ReactNode
}

export const Layout: React.FC<P> = (props) => {

    const Title = props.title

    return <>
        <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
            <div className="container">
                <NavLink className="navbar-brand" to="/">Navbar</NavLink>

                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">

                        <li className="nav-item">
                            <NavLink className="nav-link" to="music">Music</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="#">Playlists</NavLink>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>


        <div className="container">
            <div className="row">
                <div className="col">
                    <Title />

                    {props.children}

                </div>
            </div>
        </div>
    </>
}
