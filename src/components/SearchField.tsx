import React, { ChangeEvent } from 'react';

type P = {
    onSearch(query: string): void
}

type S = {
    query: string
}

/* export class SearchField extends React.Component<P, S>{
    state = { query: '' }

    handleInput = (event: ChangeEvent<HTMLInputElement>) => {
        this.setState({ query: event.currentTarget.value })
    }

    search = () => {
        this.props.onSearch(this.state.query)
    }

    componentDidMount() {
        if (this.inputRef.current) {
            this.inputRef.current.focus()
        }
    }

    inputRef = React.createRef<HTMLInputElement>()

    render() {
        return <div>
            <div className="input-group">
                <input type="text" className="form-control" onChange={this.handleInput} ref={this.inputRef} />
                <input type="button" value="Search" className="btn btn-default"
                    // onClick={() => this.props.onSearch(this.inputRef.current!.value)}
                    onClick={this.search}
                />
            </div>
        </div>
    }
} */

// <SeachField ref={inputrREf}></SeachField>

// export const SearchField = React.forwardRef<HTMLInputElement, P>(({ onSearch }, inputRef) => <div>
//     <div className="input-group">
//         <input type="text" className="form-control" ref={inputRef} />
//         <input type="button" value="Search" className="btn" onClick={() => onSearch(inputRef!.current!.value)} />
//     </div>
// </div>)


export const SearchField: React.FC<P> = (({ onSearch }) => {
    const [state, setState] = React.useState({ query: '' })

    return <div>
        <div className="input-group">
            <input type="text" className="form-control"
                // value={state.query}
                // onChange={e => onSearch(e.currentTarget.value)}
                onChange={e => setState({
                    query: e.currentTarget.value
                })}
                onKeyUp={e => e.key == 'enter' && onSearch(state.query)}
            />
            <input type="button" value="Search" className="btn" onClick={() => onSearch(state.query)} />
        </div>
    </div>
})