import React from "react";
import { Playlist } from "../../common/models";

type P = {
    playlist: Playlist, onEdit(): void
}

export const PlaylistDetails: React.FC<P> = React.memo(({ playlist, onEdit }) => <div>
    <dl
        data-playlist-id={playlist.id}
        title={playlist.name}>
        <dt>Name</dt>
        <dd>{playlist.name}</dd>

        <dt>Favorite</dt>
        <dd>{playlist.favorite ? 'Yes' : 'No'}</dd>

        <dt>Color</dt>
        <dd style={{
            color: playlist.color,
            backgroundColor: playlist.color
        }}>{playlist.color} </dd>
    </dl>
    <input type="button" value="Edit" onClick={onEdit} />
</div>)
// , (prevProps: Readonly<React.PropsWithChildren<P>>, nextProps: Readonly<React.PropsWithChildren<P>>) => boolean)