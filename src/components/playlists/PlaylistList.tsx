import React from "react";
import { Playlist } from "../../common/models";

type P = {
    playlists: Playlist[],
    selected: Playlist | null,
    onSelect(selectedId: Playlist['id']): void
}

export const PlaylistList: React.FC<P> = ({ playlists, selected, onSelect }) => <div className="list-group">
    {
        playlists.map((p, index, all) =>
            <div
                className={"list-group-item " + (p == selected ? 'active ' : '')}
                key={p.id} onClick={
                    () => onSelect(p.id)
                }>
                <span>{index + 1}. {p.name}</span>
            </div>
        )
    }
</div>
