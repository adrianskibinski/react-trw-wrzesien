import React from "react";
import { Playlist } from "../../common/models";

type P = {
    playlist: Playlist,
    onSave(draft: Playlist): void
    onCancel(): void
}
type S = {
    playlist: Playlist
}

// const validation = (thing: keyof S) => (prevState: Readonly<S>, props: Readonly<P>) => ({
//     reminingLength: 200 - prevState[thing].name.length
// })

export class PlaylistForm extends React.PureComponent<P, S> {

    constructor(props: P) {
        super(props)
        // console.log('constructor')
        this.state = {
            playlist: this.props.playlist
        }
    }

    handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const target = event.currentTarget
        const propertyKey = target.name
        const value = target.type == 'checkbox' ? target.checked : target.value;

        // ; (this.state.playlist).name = value as string
        this.setState({
            playlist: {
                ...this.state.playlist,
                [propertyKey]: value
            }
        })

    }

    render() {
        console.log('render')
        // this.validate()
        const selected = this.state.playlist
        return <form>

            <div className="form-group">
                <label>Name:</label>
                <input type="text" className="form-control" value={selected.name} name="name"
                    onChange={this.handleInput} />
                {this.state.playlist.name.length} / 200
            </div>
            <div className="form-group">
                <label>Favorite:</label>
                <input type="checkbox" checked={selected.favorite} name="favorite"
                    onChange={this.handleInput} />
            </div>
            <div className="form-group">
                <label>Color:</label>
                <input type="color" value={selected.color} name="color"
                    onChange={this.handleInput} />
            </div>

            <input type="button" value="Cancel" className="btn btn-danger" onClick={this.props.onCancel} />
            <input type="button" value="Save" className="btn btn-success"
                onClick={() => this.props.onSave(this.state.playlist)} />

        </form>
    }

    componentDidMount() {
        // console.log('componentDidMount')
    }

    static getDerivedStateFromProps(nextProp: P, nextState: S) {
        // console.log('getDerivedStateFromProps')
        return {
            playlist: nextProp.playlist.id !== nextState.playlist.id ?
                nextProp.playlist : nextState.playlist
        }
    }

    // shouldComponentUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any) {
    //     // console.log('shouldComponentUpdate')
    //     return this.props.playlist != nextProps.playlist ||
    //         this.state.playlist != nextState.playlist
    // }

    getSnapshotBeforeUpdate(prevProps: Readonly<P>, prevState: Readonly<S>) {
        // console.log('getSnapshotBeforeUpdate')
        return { placki: 123 }
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot?: any) {
        // console.log('componentDidUpdate', snapshot)
        // $('...')
    }


    componentWillUnmount() {
        // console.log('componentWillUnmount bye bye!')
    }


}