import React from "react";
import { Album } from "../../common/models/Album";

type P = {
    album: Album,
    // className?: string
} & React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>

export const AlbumCard: React.FC<P> = ({ album, className, ...props }) =>
    <div {...props} className={"card " + className}>

        <img src={album.images[0].url} className="card-img-top" />

        <div className="card-body">
            <h5 className="card-title">{album.name}</h5>
        </div>
    </div>