import React from "react";
import { SearchField } from "../components/SearchField";
import { AlbumCard } from "../components/music/AlbumCard";
import { Album } from "../common/models/Album";
import styles from './MusicSearchView.module.css'
import { searchContext, withMusicContext } from "../contexts/MusicProvider";

const SearchWithContext = withMusicContext(SearchField)

// https://github.com/acdlite/recompose/blob/master/docs/API.md
// inModalWindow(withLayout(withAutoFocus(SearchField)))

export const MusicSearchView = () => <>
    <div className="row">
        <div className="col">
            <SearchWithContext />
        </div>
    </div>
    <div className="row">
        <div className="col">
            <searchContext.Consumer>
                {({ results }) =>
                    <div className="card-group">
                        {results.map((album) =>
                            <AlbumCard title={album.name} className={styles.card_1_of_4} album={album} key={album.id}></AlbumCard>
                        )}
                    </div>
                }
            </searchContext.Consumer>
        </div>
    </div>
</>
