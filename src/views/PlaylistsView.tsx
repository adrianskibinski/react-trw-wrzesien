import React from "react";
import { PlaylistContainer } from "../containers/PlaylistsContainer";
import { SelectedPlaylist, SelectedPlaylistForm } from "../containers/SelectedPlaylist";

enum Modes { show, edit }

type State = {
    mode: Modes,
}
type Props = {}

export class PlaylistsView extends React.Component<Props, State> {

    state: State = {
        mode: Modes.show,
    }

    render() {
        return <div>
            <div className="row">
                <div className="col">
                    <PlaylistContainer />
                </div>

                <div className="col">
                    {this.state.mode == Modes.show ?
                        <SelectedPlaylist onEdit={this.edit} /> :
                        <div>Please select playlist</div>
                    }
                    {this.state.mode == Modes.edit &&
                        <SelectedPlaylistForm onCancel={this.cancel} />
                    }
                </div>
            </div>
        </div>
    }

    edit = () => {
        this.setState({ mode: Modes.edit })
    }

    cancel = () => {
        this.setState({ mode: Modes.show })
    }

}