import { connect } from 'react-redux'
import { PlaylistList } from '../components/playlists/PlaylistList';
import { PlaylistsState, selectPlaylist, selectSelectedPlaylist, selectPlaylists } from '../reducers/playlists';
import { bindActionCreators } from 'redux';

type AppState = {
    playlists: PlaylistsState
}

export const PlaylistContainer = connect(

    /* Map Store State to Component props */
    (state: AppState, ownProps: {}) => ({
        playlists: selectPlaylists(state),
        selected: selectSelectedPlaylist(state),
    }),

    /* Map Action creators to Compoment Callback Props  */
    (dispach) => bindActionCreators({
        onSelect: selectPlaylist
    }, dispach))

    /* Connect Store Via Context to stateless component  */
    (PlaylistList)

