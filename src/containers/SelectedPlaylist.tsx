import { connect } from 'react-redux'
import { PlaylistList } from '../components/playlists/PlaylistList';
import { PlaylistsState, selectPlaylist, selectSelectedPlaylist, selectPlaylists, updatePlaylist } from '../reducers/playlists';
import { bindActionCreators } from 'redux';
import { PlaylistDetails } from '../components/playlists/PlaylistDetails';
import { PlaylistForm } from '../components/playlists/PlaylistForm';
import { Playlist } from '../common/models';

type AppState = {
    playlists: PlaylistsState
}

export const ConnectSelectedPlaylist = connect(
    (state: AppState) => ({
        playlist: selectSelectedPlaylist(state)!
    }),

    (dispach) => bindActionCreators({
        onSave: updatePlaylist
    }, dispach)

    // dispatch => ({
    //     onSave(playlist: Playlist) {
    //         savePlaylistsAsync(dispatch)(playlist)
    //     }
    // })

    // dispatch => ({
    //     onSave: savePlaylistsAsync(dispatch)
    // })

    // (dispach) => bindActionCreators({
    //     onSave: savePlaylistsAsync(dispatch)
    // }, dispach)
)

export const SelectedPlaylist = ConnectSelectedPlaylist(PlaylistDetails)
export const SelectedPlaylistForm = ConnectSelectedPlaylist(PlaylistForm)

