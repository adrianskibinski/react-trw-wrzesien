import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import { PlaylistsView } from './views/PlaylistsView';
import { MusicSearchView } from './views/MusicSearchView';
import { Layout } from './components/Layout';
import { Route, Switch, Redirect } from 'react-router-dom';

const Title = () => <h1>Awesome</h1>

const App: React.FC = () => {
  return (

    <Layout title={Title}>
      {/* <MusicSearchView></MusicSearchView> */}
      {/* <PlaylistsView></PlaylistsView> */}

      <Switch>
        {/* <Route path="/" exact={true} component={MusicSearchView} /> */}
        <Redirect path="/" exact={true} to="/music" />

        <Route path="/music" component={MusicSearchView} />
        <Route path="/playlists" component={PlaylistsView} />

        <Route path="**" render={() => <div>404 NOT FOUND</div>} />
      </Switch>

    </Layout>

  );
}

export default App;
