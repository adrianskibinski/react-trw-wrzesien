import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { auth } from './services';
import { MusicProvider } from './contexts/MusicProvider';
import { store } from './store';
import { loadPlaylists } from './reducers/playlists';
import { Provider } from 'react-redux';
// import * as serviceWorker from './serviceWorker';

// import { HashRouter as Router } from 'react-router-dom'
import { BrowserRouter as Router } from 'react-router-dom'

auth.getToken();

(window as any).loadPlaylists = loadPlaylists;
(window as any).store = store;


ReactDOM.render(
    <Router>
        <Provider store={store}>
            <MusicProvider>
                <App />
            </MusicProvider>
        </Provider>
    </Router>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
