import { Auth } from "./services/auth";
import { MusicSearch } from "./services/MusicSearch";
import React from "react";
import { Album } from "./common/models/Album";

export const auth = new Auth(
    'https://accounts.spotify.com/authorize',
    'd6ef823bb7e446f9b0df5be68ae82ea4',
    'http://localhost:3000/'
)

export const search = new MusicSearch()

