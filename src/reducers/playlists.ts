import { Reducer, ActionCreator, Action, Dispatch } from "redux";
import { Playlist } from "../common/models";

export type PlaylistsState = {
    items: Playlist[]
    selectedId: Playlist['id'] | null
}

const initialState: PlaylistsState = {
    items: [
        {
            id: 123,
            name: 'React Hits',
            favorite: true,
            color: '#ff00ff'
        }, {
            id: 234,
            name: 'React TOP20',
            favorite: false,
            color: '#ffff00'
        }, {
            id: 345,
            name: 'BEST of React',
            favorite: false,
            color: '#00ffff'
        }],
    selectedId: 123
}

export const playlistsReducer: Reducer<PlaylistsState, PlaylistsActions> = (
    state = initialState, action
) => {

    switch (action.type) {
        case LOAD_PLAYLISTS: return { ...state, items: action.payload }

        case SELECT_PLAYLIST: return { ...state, selectedId: action.payload.id }

        case UPDATE_PLAYLIST: {
            // updateItemHelper(state,'items',action.payload)
            const draft = action.payload
            return { ...state, items: state.items.map(p => p.id === draft.id ? draft : p) }
        }
        default:
            return state;
    }
}


/* enum ActionTypes {
    LOAD_PLAYLISTS = '[Playlists] Load Playlists',
    SELECT_PLAYLIST = '[Playlists] Select Playlist',
    UPDATE_PLAYLIST = '[Playlists] Update Playlist',
} */

export const LOAD_PLAYLISTS = '[Playlists] Load Playlists';
export const SELECT_PLAYLIST = '[Playlists] Select Playlist';
export const UPDATE_PLAYLIST = '[Playlists] Update Playlist';

interface LoadPlaylists extends Action<typeof LOAD_PLAYLISTS> { payload: Playlist[] }
interface SelectPlaylists extends Action<typeof SELECT_PLAYLIST> { payload: { id: Playlist['id'] } }
interface UpdatePlaylists extends Action<typeof UPDATE_PLAYLIST> { payload: Playlist }

type PlaylistsActions = LoadPlaylists | SelectPlaylists | UpdatePlaylists

export const loadPlaylists: ActionCreator<LoadPlaylists> = (payload: Playlist[]) => ({
    type: LOAD_PLAYLISTS,
    payload
})

export const selectPlaylist: ActionCreator<SelectPlaylists> = (id: Playlist['id']) => ({
    type: SELECT_PLAYLIST, payload: { id }
})

export const updatePlaylist: ActionCreator<UpdatePlaylists> = (payload: Playlist) => ({
    type: UPDATE_PLAYLIST, payload
})

/* Selector */

type AppState = {
    playlists: PlaylistsState
}

export const selectPlaylistsState = (s: AppState) => s.playlists

export const selectPlaylists = (s: AppState) => selectPlaylistsState(s).items

export const selectSelectedPlaylist = (s: AppState) => {
    const ps = selectPlaylistsState(s)
    return ps.items.find(p => p.id == ps.selectedId) || null
}


// interface StartPlaylsitsLoadin extends Action<typeof UPDATE_PLAYLIST> { payload: Playlist }

// export const savePlaylistsAsync = (dispatch: Dispatch) => (draft: Playlist) => {
//     // const dispatch = store.dispach()

//     dispatch(startLoading())

//     fetch('', { body: draft }).then(r => r.json()).then(resp => {
//         dispatch(loadPlaylists(resp))
//     }, err => {
//         dispatch(playlistError())
//     })

// }